const buttons = document.querySelectorAll('.button-show');

buttons.forEach(function (button) {
    let orderId = button.getAttribute('data-orderId');
    let popup = document.querySelector('#popup-wrapper-'+orderId);
    let close = document.querySelector('#popup-close-'+orderId)
    button.addEventListener('click', () => {
        popup.style.display = 'block';
    });
    close.addEventListener('click', () => {
        popup.style.display = 'none';
    });
    popup.addEventListener('click', e => {
        if(e.target.className === 'popup-wrapper') {
            popup.style.display = 'none';
        }
    });
});