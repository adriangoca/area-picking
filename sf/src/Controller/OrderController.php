<?php

namespace App\Controller;

use App\Entity\Order;
use App\Form\OrderType;
use App\Repository\OrderRepository;
use Doctrine\ORM\EntityManagerInterface;
use GuzzleHttp\Client;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;

class OrderController extends AbstractController
{
    /**
     * @Route("/order/list", name="order_list")
     */
    public function index(EntityManagerInterface $entityManager): Response
    {
        $repository = $entityManager->getRepository(Order::class);
        $orders = $repository->findAll();

        return $this->render('order/index.html.twig', [
            'orders' => $orders,
            'lastCheck' => (new \DateTime())->getTimestamp(),
            'buttonTopHref' => $this->generateUrl('order_show_only_accepted_payment_and_sent'),
            'buttonTopText' => 'Show only orders in accepted payment or sent states',
        ]);
    }

    /**
     * @Route(
     *     "/order/check-new-orders",
     *     name="order_check_news",
     *     methods={"POST"},
     * )
     */
    public function checkNewOrders(Request $request, EntityManagerInterface $entityManager): JsonResponse
    {
        $lastCheck = $request->get('lastCheck');
        /** @var OrderRepository $repository */
        $repository = $entityManager->getRepository(Order::class);
        $orders = $repository->findByDateAddAboveTimestamp($lastCheck);

        return new JsonResponse(['has_new_orders' => !empty($orders)]);
    }

    /**
     * @Route("/order/show-only-accepted-payment-and-sent", name="order_show_only_accepted_payment_and_sent")
     */
    public function showOnlyAcceptedPaymentAndSentStates(EntityManagerInterface $entityManager)
    {
        /** @var OrderRepository $repository */
        $repository = $entityManager->getRepository(Order::class);
        $orders = $repository->findByAcceptedPaymentAndSentStates();

        return $this->render('order/index.html.twig', [
            'orders' => $orders,
            'lastCheck' => (new \DateTime())->getTimestamp(),
            'buttonTopHref' => $this->generateUrl('order_list'),
            'buttonTopText' => 'Show all orders',
        ]);
    }

    /**
     * @Route("/order/edit-state/{orderId}", name="order_edit_state")
     */
    public function editOrderState(Request $request): Response
    {
        $orderId = $request->get('orderId');
        $repository = $this->getDoctrine()->getRepository(Order::class);
        $order = $repository->find($orderId);
        $form = $this->createForm(OrderType::class, $order);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $order = $form->getData();
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($order);
            $entityManager->flush();

            $this->addFlash('notice', 'The order state has been updated');

            return $this->redirectToRoute('order_list');
        }

        return $this->render('order/edit_state.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/order/show/{orderId}", name="order_show")
     */
    public function showOrder(Request $request, Client $client): Response
    {
        $key = $this->getParameter('prestashop_api_key');
        $host = $this->getParameter('prestashop_host');
        $orderId = $request->get('orderId');
        $response = $client->get("http://$key@$host/api/orders/$orderId?&output_format=JSON");

        if ($response->getStatusCode() != Response::HTTP_OK) {
            $this->createNotFoundException();
        }

        return $this->render('order/show.html.twig', [
            'orderId' => $orderId,
            'json_format' => $response->getBody()->getContents(),
        ]);
    }

    /**
     * @Route("/order/cancel/{orderId}", name="order_cancel")
     */
    public function cancelOrder(Request $request, Client $client)
    {
        $key = $this->getParameter('prestashop_api_key');
        $host = $this->getParameter('prestashop_host');
        $orderId = $request->get('orderId');
        $response = $client->get("http://$key@$host/api/orders/$orderId");

        if ($response->getStatusCode() != Response::HTTP_OK) {
            $this->createNotFoundException();
        }

        $rawXml = $response->getBody()->getContents();
        $document = new \DOMDocument('1.0', 'utf-8');
        $document->loadXML($rawXml);
        $xpath = new \DOMXPath($document);
        $elements = $xpath->query('//current_state');
        if ($elements->length >= 1) {
            $element = $elements->item(0);
            $element->removeChild($element->firstChild);
            $cdata = $document->createCDATASection('6');
            $element->appendChild($cdata);
        }
        $updatedXML = $document->saveXML();

        $response = $client->put("http://$key@$host/api/orders/$orderId", [
            'body' => $updatedXML
        ]);

        if ($response->getStatusCode() != Response::HTTP_OK) {
            return new BadRequestHttpException();
        }

        $this->addFlash('notice', 'The order state has been updated');

        return $this->redirectToRoute('order_list');
    }
}
