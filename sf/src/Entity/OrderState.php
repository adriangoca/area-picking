<?php

namespace App\Entity;

use App\Repository\OrderStateRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=OrderStateRepository::class)
 * @ORM\Table(name="`ps_order_state`")
 */
class OrderState
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", name="id_order_state")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity=OrderStateLanguage::class, mappedBy="orderState", orphanRemoval=true)
     */
    private $orderStateLanguages;

    public function __construct()
    {
        $this->orderStateLanguages = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|OrderStateLanguage[]
     */
    public function getOrderStateLanguages(): Collection
    {
        return $this->orderStateLanguages;
    }

    public function addOrderStateLanguage(OrderStateLanguage $orderStateLanguage): self
    {
        if (!$this->orderStateLanguages->contains($orderStateLanguage)) {
            $this->orderStateLanguages[] = $orderStateLanguage;
            $orderStateLanguage->setOrderState($this);
        }

        return $this;
    }

    public function removeOrderStateLanguage(OrderStateLanguage $orderStateLanguage): self
    {
        if ($this->orderStateLanguages->removeElement($orderStateLanguage)) {
            // set the owning side to null (unless already changed)
            if ($orderStateLanguage->getOrderState() === $this) {
                $orderStateLanguage->setOrderState(null);
            }
        }

        return $this;
    }
}
