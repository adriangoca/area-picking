<?php

namespace App\Entity;

use App\Repository\ProductLanguageRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ProductLanguageRepository::class)
 * @ORM\Table(name="`ps_product_lang`")
 */
class ProductLanguage
{
    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity=Product::class, inversedBy="productLanguages")
     * @ORM\JoinColumn(nullable=false, referencedColumnName="id_product", name="id_product")
     */
    private $product;

    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity=Language::class)
     * @ORM\JoinColumn(nullable=false, referencedColumnName="id_lang", name="id_lang")
     */
    private $Language;

    /**
     * @ORM\Column(type="string", length=128)
     */
    private $name;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function getLanguage(): ?Language
    {
        return $this->Language;
    }

    public function setLanguage(?Language $Language): self
    {
        $this->Language = $Language;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }
}
