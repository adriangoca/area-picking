<?php

namespace App\Entity;

use App\Repository\ProductRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ProductRepository::class)
 * @ORM\Table(name="`ps_product`")
 */
class Product
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", name="id_product")
     */
    private $id;

    /**
     * @ORM\Column(type="decimal", precision=26, scale=6)
     */
    private $price;

    /**
     * @ORM\OneToMany(targetEntity=ProductLanguage::class, mappedBy="product", orphanRemoval=true)
     */
    private $productLanguages;

    public function __construct()
    {
        $this->productLanguages = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPrice(): ?string
    {
        return $this->price;
    }

    public function setPrice(string $price): self
    {
        $this->price = $price;

        return $this;
    }

    /**
     * @return Collection|ProductLanguage[]
     */
    public function getProductLanguages(): Collection
    {
        return $this->productLanguages;
    }

    public function addProductLanguage(ProductLanguage $productLanguage): self
    {
        if (!$this->productLanguages->contains($productLanguage)) {
            $this->productLanguages[] = $productLanguage;
            $productLanguage->setProduct($this);
        }

        return $this;
    }

    public function removeProductLanguage(ProductLanguage $productLanguage): self
    {
        if ($this->productLanguages->removeElement($productLanguage)) {
            // set the owning side to null (unless already changed)
            if ($productLanguage->getProduct() === $this) {
                $productLanguage->setProduct(null);
            }
        }

        return $this;
    }
}
