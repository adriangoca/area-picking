<?php

namespace App\Entity;

use App\Repository\CountryLanguageRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CountryLanguageRepository::class)
 * @ORM\Table(name="`ps_country_lang`")
 */
class CountryLanguage
{
    /**
     * @ORM\Column(type="string", length=64)
     */
    private $name;

    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity=Country::class, inversedBy="countryLanguages")
     * @ORM\JoinColumn(nullable=false, referencedColumnName="id_country", name="id_country")
     */
    private $country;

    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity=Language::class)
     * @ORM\JoinColumn(nullable=false, referencedColumnName="id_lang", name="id_lang")
     */
    private $language;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCountry(): ?Country
    {
        return $this->country;
    }

    public function setCountry(?Country $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getLanguage(): ?Language
    {
        return $this->language;
    }

    public function setLanguage(?Language $language): self
    {
        $this->language = $language;

        return $this;
    }
}
