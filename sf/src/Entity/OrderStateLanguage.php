<?php

namespace App\Entity;

use App\Repository\OrderStateLanguageRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=OrderStateLanguageRepository::class)
 * @ORM\Table(name="`ps_order_state_lang`")
 */
class OrderStateLanguage
{
    /**
     * @ORM\Column(type="string", length=64)
     */
    private $name;

    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity=OrderState::class, inversedBy="orderStateLanguages")
     * @ORM\JoinColumn(nullable=false, referencedColumnName="id_order_state", name="id_order_state")
     */
    private $orderState;

    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity=Language::class)
     * @ORM\JoinColumn(nullable=false, referencedColumnName="id_lang", name="id_lang")
     */
    private $language;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getOrderState(): ?OrderState
    {
        return $this->orderState;
    }

    public function setOrderState(?OrderState $orderState): self
    {
        $this->orderState = $orderState;

        return $this;
    }

    public function getLanguage(): ?Language
    {
        return $this->language;
    }

    public function setLanguage(?Language $language): self
    {
        $this->language = $language;

        return $this;
    }
}
