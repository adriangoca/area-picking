<?php

namespace App\Entity;

use App\Repository\CountryRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CountryRepository::class)
 * @ORM\Table(name="`ps_country`")
 */
class Country
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", name="id_country")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity=CountryLanguage::class, mappedBy="country", orphanRemoval=true)
     */
    private $countryLanguages;

    public function __construct()
    {
        $this->countryLanguages = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|CountryLanguage[]
     */
    public function getCountryLanguages(): Collection
    {
        return $this->countryLanguages;
    }

    public function addCountryLanguage(CountryLanguage $countryLanguage): self
    {
        if (!$this->countryLanguages->contains($countryLanguage)) {
            $this->countryLanguages[] = $countryLanguage;
            $countryLanguage->setCountry($this);
        }

        return $this;
    }

    public function removeCountryLanguage(CountryLanguage $countryLanguage): self
    {
        if ($this->countryLanguages->removeElement($countryLanguage)) {
            // set the owning side to null (unless already changed)
            if ($countryLanguage->getCountry() === $this) {
                $countryLanguage->setCountry(null);
            }
        }

        return $this;
    }
}
