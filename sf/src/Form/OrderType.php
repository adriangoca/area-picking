<?php

namespace App\Form;

use App\Entity\Order;
use App\Entity\OrderState;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OrderType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('state', EntityType::class, [
                'class' => OrderState::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('os')
                        ->orderBy('os.id', 'ASC');
                },
                'choice_label' => function (OrderState $orderState) {
                    return $orderState->getOrderStateLanguages()->first()->getName();
                },
            ])
            ->add('save', SubmitType::class, ['label' => 'Save'])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Order::class,
        ]);
    }
}
