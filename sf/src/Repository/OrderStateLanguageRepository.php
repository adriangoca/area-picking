<?php

namespace App\Repository;

use App\Entity\OrderStateLanguage;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method OrderStateLanguage|null find($id, $lockMode = null, $lockVersion = null)
 * @method OrderStateLanguage|null findOneBy(array $criteria, array $orderBy = null)
 * @method OrderStateLanguage[]    findAll()
 * @method OrderStateLanguage[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrderStateLanguageRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OrderStateLanguage::class);
    }

    // /**
    //  * @return OrderStateLanguage[] Returns an array of OrderStateLanguage objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?OrderStateLanguage
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
