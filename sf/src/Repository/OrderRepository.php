<?php

namespace App\Repository;

use App\Entity\Order;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Order|null find($id, $lockMode = null, $lockVersion = null)
 * @method Order|null findOneBy(array $criteria, array $orderBy = null)
 * @method Order[]    findAll()
 * @method Order[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrderRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Order::class);
    }

    /**
     * @param int $timestamp
     * @return Order[]
     */
    public function findByDateAddAboveTimestamp(int $timestamp)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.dateAdd > :timestamp')
            ->setParameter('timestamp', date('Y-m-d H:i:s', $timestamp))
            ->getQuery()
            ->getResult();
    }

    /**
     * @return Order[]
     */
    public function findByAcceptedPaymentAndSentStates()
    {
        return $this->createQueryBuilder('o')
            ->where('o.state = :acceptedPaymentState')
            ->orWhere('o.state = :sentState')
            ->setParameter('acceptedPaymentState', 2)
            ->setParameter('sentState', 4)
            ->getQuery()
            ->getResult();
    }

    // /**
    //  * @return Order[] Returns an array of Order objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Order
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
