Area Picking
============

Case study made with Prestashop 1.7 and Symfony 4.4

Folder structure
----------------

```
|── .docker
|── data
|── sf
    ├── (Symfony files)
├── web
│   ├── (Prestashop files)
|── .gitignore
├── README.md
└── docker-compose.yml

```
The **.docker** folder contains dockerfiles and configuration files to build the docker images.

The **data** folder stores all database files. It is ignored and not distributed in the proyect.

The **sf** folder contains a Symfony project with the web application requested by the case study.

The **web** folder contains those files generated after a Prestashop installation.

Network and instances
---------------------

- **172.21.0.2**: Mysql server with the Prestashop database.
- **172.21.0.3**: Prestashop web server.
- **172.21.0.4**: PhpMyAdmin server connected to Prestashop database for consulting purposes.
- **172.21.0.5**: Apache web server with the application based on Symfony
- **172.21.0.6**: PHP instance linked to the Apache web server

To start all server on the network, run docker-compose:

```bash
docker-compose up -d
```

Results
-------

The main page (located at /order/list) looks like follows:

![image.png](./orders_list.png)

A live preview can be seen through an Ngrok tunneling link. Please, ask for it when needed.
